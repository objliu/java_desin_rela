package com.lzj.shejimoshi.observer;

public class Teacher implements Observer{
	private String name;
	

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	@Override
	public void update(Subject su) {
	
		System.out.println(((Student)su).getName()+"签到了");
		
	}

}
