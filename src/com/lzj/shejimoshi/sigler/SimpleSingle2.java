package com.lzj.shejimoshi.sigler;
/**
 * 懒汉模式 多线程下安全的（不是绝对安全）Java5这个问题修复了
 * @author Administrator
 *
 */
public class SimpleSingle2 {

	private SimpleSingle2() {
		// TODO Auto-generated constructor stub
	}
	public static volatile SimpleSingle2 instance;
	
	public static SimpleSingle2 getInstance(){
		
		if(instance==null){
			synchronized(SimpleSingle2.class){
				if (instance==null) {
					instance=new SimpleSingle2();
				}
			}
		}
		return instance;
	}
	
}
