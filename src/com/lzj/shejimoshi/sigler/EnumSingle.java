package com.lzj.shejimoshi.sigler;
/**
 * 枚举实现单例模式：线程安全，不会因为序列化产生多个实例,防反射攻击
 * @author Administrator
 *
 */
public enum EnumSingle {

	single(){

		@Override
		public EnumSingle getInstanc() {
			// TODO Auto-generated method stub
			return single;
		}
		
	};
	
	private String  name;
	private int age;
	private String contentInfo;
	
	public String getContentInfo() {
		return contentInfo;
	}
	public void setContentInfo(String contentInfo) {
		this.contentInfo = contentInfo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public abstract EnumSingle getInstanc();
	private EnumSingle(){
		
	}
}
