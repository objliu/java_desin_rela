package com.lzj.shejimoshi.observer;

public interface Subject {
	public void attach(Observer observer);
	public void dettach(Observer observer);
	public void notifyObserver(Subject su);
}
