package com.lzj.shejimoshi.Builder;

/**
 * 构建者模式
 * @author Administrator
 *
 */
public class Configration {
	private String ip;
	private int port;
	private String name;
	private String Content;
	
	
	public String getIp() {
		return ip;
	}
	public int getPort() {
		return port;
	}
	public String getName() {
		return name;
	}
	public String getContent() {
		return Content;
	}
	private Configration() {
	}
	public static class Builder{
		private String ip;
		private int port;
		private String name;
		private String Content;
		public String getIp() {
			return ip;
		}
		public Builder setIp(String ip) {
			this.ip = ip;
			return this;
		}
		public int getPort() {
			return port;
		}
		public Builder setPort(int port) {
			this.port = port;
			return this;
		}
		public String getName() {
			return name;
		}
		public Builder setName(String name) {
			this.name = name;
			return this;
		}
		public String getContent() {
			return Content;
		}
		public Builder setContent(String content) {
			Content = content;
			return this;
		}
		public Configration build(){
			Configration config = new Configration();
			config.ip=this.ip;
			config.Content=this.Content;
			config.name=this.name;
			config.port=this.port;
			return config;
			
		}
		
	}
}
