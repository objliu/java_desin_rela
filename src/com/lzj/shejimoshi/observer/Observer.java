package com.lzj.shejimoshi.observer;

public interface Observer {
	public void update(Subject su);
}
