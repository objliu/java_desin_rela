package com.lzj.shejimoshi;

import com.lzj.shejimoshi.Builder.Configration;
import com.lzj.shejimoshi.observer.Student;
import com.lzj.shejimoshi.observer.Teacher;
import com.lzj.shejimoshi.sigler.EnumSingle;

public class Test {

	public static void main(String[] args) {
		EnumSingle single=EnumSingle.single.getInstanc();
		single.setName("小年");
		System.out.println(single.getName());
		EnumSingle single2=EnumSingle.single.getInstanc();
		System.out.println(single2.getName());
		single2.setName("小红");
		System.out.println(single.getName());
		
		
		Configration config = new Configration.Builder().setIp("192.168.0.1").setName("networdk").setPort(5000).setContent("config_content").build();
		System.out.println(config.getName()+"====="+config.getContent());
		
		Teacher t=new Teacher();
		t.setName("老王");
		Student stu=new Student();
		stu.attach(t);
		stu.setName("小红");
		stu.sigin();
		Student stu2=new Student();
		stu2.attach(t);
		stu2.setName("小明");
		stu2.sigin();
	
	}
}
