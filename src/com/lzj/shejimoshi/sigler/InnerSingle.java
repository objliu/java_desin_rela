package com.lzj.shejimoshi.sigler;

/**
 * 内部类实现方式（优雅的线程安全，随调随用）
 * @author Administrator
 *
 */
public class InnerSingle {

	private InnerSingle() {
		// TODO Auto-generated constructor stub
	}
	
	private static class SingleInstance{
		private static final InnerSingle instance=new InnerSingle();
	}
	
	public static InnerSingle getInstance(){
		return SingleInstance.instance;
	}
}
