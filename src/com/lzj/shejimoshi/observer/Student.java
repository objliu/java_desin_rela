package com.lzj.shejimoshi.observer;

import java.util.ArrayList;
import java.util.Vector;

public class Student implements Subject{

	private ArrayList<Observer> observersVector = new ArrayList<Observer>();
	
	private String name;
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void attach(Observer observer) {
		// TODO Auto-generated method stub
		observersVector.add(observer);
	}

	@Override
	public void dettach(Observer observer) {
		// TODO Auto-generated method stub
		observersVector.remove(observer);
	}

	@Override
	public void notifyObserver(Subject su) {
		for (Observer observer : observersVector) {
			observer.update(su);
		}
		
	}
	
	/**
	 * 签到
	 */
	public void sigin(){
		notifyObserver(this);
	}

}
